FROM rust

# Check for architecture and install cargo-deny accordingly
RUN ARCH=$(uname -m); \
    if [ "$ARCH" = "x86_64" ]; then \
        export LATEST_DENY_VERSION=$(git ls-remote --tags --sort="v:refname" --refs https://github.com/EmbarkStudios/cargo-deny.git | tail -n1 | awk '{print $2}' | awk -F '/' '{print $3}'); \
        export APP_NAME=cargo-deny-${LATEST_DENY_VERSION}-${ARCH}-unknown-linux-musl; \
        curl -LJO https://github.com/EmbarkStudios/cargo-deny/releases/download/${LATEST_DENY_VERSION}/${APP_NAME}.tar.gz; \
        tar -xzf ${APP_NAME}.tar.gz; \
        mv ${APP_NAME}/cargo-deny ./bin/; \
    elif [ "$ARCH" = "aarch64" ]; then \
        cargo install cargo-deny; \
    else \
        echo "Unsupported architecture: $ARCH"; \
        exit 1; \
    fi;

# move target dir out of project folder
RUN mkdir /target
ENV CARGO_TARGET_DIR=/target
WORKDIR /project
CMD ["cargo", "deny", "--all-features", "check"]